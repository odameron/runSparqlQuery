# runSparqlQuery

A simple script for running a [SPARQL](https://www.w3.org/TR/sparql11-overview/) query on a remote endpoint.


## Usage

`runSparqlQuery.bash -q queryPath [-u endpointURL | -n endpointName] [-f <csv|json>]`


## Example

`runSparqlQuery.bash -q examples/uniprotNameToID.rq -n uniprot -f csv`


## Additional scripts

I also needed to compare the results of [Apache jena](https://jena.apache.org/) and [python RDFLib](https://github.com/RDFLib/rdflib). 
The scripts `runQuery-jena` and `runQuery-rdflib` are simple wrapper scripts to simplify and unify the syntax. I also added a (crude) display of the script execution time.

`runQuery-XXX pathToRdfFile pathToSparqlQuery`


## Todo

completed (so far)
