#! /usr/bin/env bash

# TODO: usage: $0 -q queryPath [-u endpointURL | -n endpointName] [-f <csv|json>]

declare -A endpoints
endpoints["localhost"]="http://localhost/sparql" # DEFAULT, probably leads nowhere
endpoints["localFuseki"]="http://localhost:3030/SERVICE_NAME/query"
endpoints["localVirtuoso"]="http://localhost:8080/sparql"
#endpoints["uniprot"]="http://sparql.uniprot.org/sparql"
endpoints["uniprot"]="https://sparql.uniprot.org/sparql"
#endpoints["nextprot"]="https://snorql.nextprot.org/sparql"
endpoints["nextprot"]="https://sparql.nextprot.org/sparql"
endpoints["ebi"]="https://www.ebi.ac.uk/rdf/services/sparql"
endpoints["disgenet"]="http://rdf.disgenet.org/sparql/"
endpoints["dbpedia"]="http://dbpedia.org/sparql"
endpoints["rhea"]="http://sparql.rhea-db.org/sparql"

#endpointURL=${endpoints["uniprot"]}
#endpointURL=${endpoints["rhea"]}
endpointURL=${endpoints["localhost"]}


function displayUsage {
  echo "Usage:"
  echo "  $0 (-h | --help)"
  echo "  $0 (-e | --list-endpoints)"
  echo "  $0 (-q | --query-path) <queryPath> [ (-u | --endpoint-url) <endpointURL> | (-n | --endpoint-name) <endpointName> ] [ (-f | --answer-format) <csv|json> ]"
}

function displayHelp {
  displayUsage
  echo ""
  echo "Arguments: "
  echo "  -h | --help this message"
  echo "  -e | --list-endpoints lists the name and the URL of the known endpoints"
  echo "  -q | --query-path path to a (local) file containing the SPARQL query"
  echo "  -u | --endpoint-url the URL of the endpoint where to send the SPARQL query"
  echo "  -n | --endpoint-name the name of the endpoint where to send the SPARQL query (cf. -e or --list-endpoints options)"
  echo "  -f | --answer-format the format the SPARQL endpoint is supposed to return its answer in (default = csv)"
  echo ""
  echo "If neither -u nor -n is provided, the default endpoint is localhost"
}

function displayEndpointsList {
  echo "Known endpoints:"
  for currentEndpoint in "${!endpoints[@]}"
  do
    echo "  ${currentEndpoint}:	${endpoints[${currentEndpoint}]}"
  done | sort
}

if [ "$#" -eq 0 ]; then
	displayUsage
	exit 1 # wrong number of arguments
elif [ "$#" -eq 1 ]; then
	case $1 in
		"-h" | "--help" )
			displayHelp
			exit 0
			;;
		"-l" | "--list-endpoints" )
			displayEndpointsList
			exit 0
			;;
		* )
			echo "$0: unknown option"
			displayUsage
			exit 2 # wrong type of argument
	esac
else
	queryPath="query.rq"
	resultMimetype="text/csv"
	while [ -n "$1" ]; 
	do
		case "$1" in
			"-q" | "--query-path" )
				queryPath=$2
				shift 2
				;;
			"-u" | "--endpoint-url" )
				endpointURL=$2
				shift 2
				;;
			"-n" | "--endpoint-name" )
				endpointURL=${endpoints[$2]}
				shift 2
				;;
			"-f" | "--answer-format" )
				case "$2" in
					"csv" )
						resultMimetype="text/csv"
						;;
					"json" )
						#resultMimetype="application/json"
						resultMimetype="application/sparql-results+json"
						;;
					* )
						echo "Unnknown SPARQL answer format"
						exit 3
				esac
				shift 2
				;;
			* )
				echo "$0: unsupported option $1"
				displayUsage
				exit 3
		esac
	done
fi


query=`cat ${queryPath}`

#resultMimetype="text/csv"
#resultMimetype="application/sparql-results+json"

#curl --header "Accept: ${resultMimetype}" -G 'http://sparql.uniprot.org/sparql' --data-urlencode query="${query}"
curl --header "Accept: ${resultMimetype}" -G "${endpointURL}" --data-urlencode query="${query}"

